import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class program_1 {
    public static void main(String[] args) {
        String filePath = "C:\\Users\\Ajay\\OneDrive\\Desktop\\worldline\\Assignment_01\\assign_01\\input.txt";
        try {
            FileReader fileReader = new FileReader(filePath);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            String username = bufferedReader.readLine();
            if (username != null && !username.isEmpty()) {
                System.out.println("Hello " + username + "!");
            } else {
                System.out.println("No username found in input file.");
            }
            bufferedReader.close();
        } catch (IOException e) {
            System.err.println("Error reading input file: " + e.getMessage());
        }
    }
}
