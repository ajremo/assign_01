# assign_01
# Objective
The objective of this assignment is to familiarize yourself with setting up a Continuous Integration/Continuous Deployment (CI/CD) pipeline using GitLab CI/CD and jenkins. You will create a simple pipeline that automates the Deployment process of a sample web / desktop application.

# instructions
This repository provides instructions and configuration files for setting up a CI/CD pipeline using GitLab CI/CD and Jenkins.
It includes the following steps :
1. Clone the repository
2. Develope your application
3. Configure the pipeline
4. Connect to Jenkins

The process involves creating a '.gitlab-ci.yml' file, configuring deployment and testing the pipeline.

# description of program
The objective of the code is to read a username from a text file (input.txt) and greet the user by printing "Hello, [username]!" to the console. If the username is not found in the file or if an error occurs during the file reading process, appropriate error messages are displayed.

# prerequisites
git installed on your local machine
access to gitlab account
access to a jenkins server

# troubleshooting
proper configuration in the '.gitlab-ci.yml' file
refer to documentation
